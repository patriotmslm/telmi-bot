const fs = require('fs');

/**
 * Windows C++ Node.js addon
 * Implements COM IDispatch object wrapper
 * Analog ActiveXObject on script.exe
 */
require('winax');

console.log('Downloading tickers from amibroker');
const AB = new ActiveXObject('Broker.Application');
const stock = AB.Stocks();
const stocksCount = AB.Stocks.Count;
const tickers = [];

for (let i = 0; i < stocksCount; i++) {
    const ticker = stock.Item(i).Ticker;
    tickers.push(`${ticker}`);
}

const dir = './amibroker';
if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir);
}

fs.writeFile('./amibroker/tickers.json', JSON.stringify(tickers), (err) => {
    if (err) {
        console.error(err);
        return;
    }

    console.log(`${stocksCount} tickers have been sucessfully saved`);
});