const { DateTime } = require('luxon');
const path = require('path');
const fs = require('fs');
const tickers = require('../amibroker/tickers.json');

exports.getImage = (bot, msg, match, sheetIndex) => {
  const chatId = msg.chat.id;
  const tickerCode = match[1].toUpperCase();
  
  try {
    const ticker = tickers.find((t) => t === tickerCode);
  
    if (!ticker) {
      return bot.sendMessage(chatId, `Chart request could not found ${tickerCode}. Please check the ticker code and try again.`);
    }

    const imgPath = path.resolve(`amibroker/${DateTime.now().toMillis()}-${chatId}.png`);
    const amibroker = new ActiveXObject('Broker.Application');
    // Open window with specific ticker
    amibroker.ActiveDocument.Name = ticker;
    const activeWindow = amibroker.ActiveWindow;
    // Select specific tab
    activeWindow.SelectedTab = sheetIndex;
    // Export as image
    activeWindow.ExportImage(imgPath, 1080, 608);

    //Send an image to chat
    bot.sendPhoto(chatId, imgPath, {
      caption: `${tickerCode}. Date: ${DateTime.now().toISODate()}`
    });

    // Unlink temporary file
    fs.unlink(imgPath, () => {});
  } catch (error) {
    console.error(error);
  }
};