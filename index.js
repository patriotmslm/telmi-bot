const TelegramBot = require('node-telegram-bot-api');

/**
 * Windows C++ Node.js addon
 * Implements COM IDispatch object wrapper
 * Analog ActiveXObject on script.exe
 */
require('winax');

/**
 * Load environment variables from .env file, where bot is configured
 */
require('dotenv').config();

/**
 * Controllers (command handlers)
 */
const chartController = require('./controllers/chart')

/**
 * Create a bot that uses 'polling' to fetch new updates
 */
const bot = new TelegramBot(process.env.TELEGRAM_TOKEN, {polling: true});

/**
 * Primary bot commands
 */
bot.onText(/\/ichimoku (.+)/, (msg, match) => chartController.getImage(bot, msg, match, process.env.SHEET_INDEX_ICHIMOKU));
bot.onText(/\/pattern (.+)/, (msg, match) => chartController.getImage(bot, msg, match, process.env.SHEET_INDEX_PATTERN));
bot.onText(/\/demand (.+)/, (msg, match) => chartController.getImage(bot, msg, matc, process.env.SHEET_INDEX_DEMAND));
bot.onText(/\/equivol (.+)/, (msg, match) => chartController.getImage(bot, msg, match, process.env.SHEET_INDEX_EQUIVOL));
bot.onText(/\/fractal (.+)/, (msg, match) => chartController.getImage(bot, msg, match, process.env.SHEET_INDEX_FRACTAL));
bot.onText(/\/pixel (.+)/, (msg, match) => chartController.getImage(bot, msg, match, process.env.SHEET_INDEX_PIXEL));
bot.onText(/\/guppy (.+)/, (msg, match) => chartController.getImage(bot, msg, match, process.env.SHEET_INDEX_GUPPY));